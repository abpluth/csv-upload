# CSV Upload

## Instructions
Start server with:
```bash
docker-compose up -d
```
Visit [localhost:3000/docs](http://localhost:3000/docs) to view docs and interact with the api endpoints.

## Assumptions:
- All providers will provide a csv with the specified columns. If that is not the case, a mapping will be required for providers which differ from the default.
- It is mentioned that different providers will provide different fields, but it is not indicated whether we need to store these extraneous fields. The assumption was made that these fields can be ignored. If that is not the case, a metadata field could be added to store the extraneous fields. Alternatively, data for different providers could be stored in separate tables.

# TODO:
- [ ] add error handling
