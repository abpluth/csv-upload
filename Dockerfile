FROM node:alpine3.16 as build

WORKDIR /app

COPY ./ /app/
RUN npm install --frozen-lockfile

EXPOSE 3000
CMD ["node", "src/server.js"]
