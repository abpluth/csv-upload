const path = require('path')
const request = require('supertest')
const app = require('../src/app')
const db = require('../src/db')
const { createSchemas } = require('../src/db/schemas')
const knex = require('../src/db')

describe('Test /api/cars routes', () => {
    beforeAll(async () => {
        await createSchemas(knex)
    })

    afterAll(async () => {
        await db.destroy()
        // await app.close()
    })

    const filePath = `${path.resolve()}/test/sample.csv`

    test('POST /api/upload - Uploads a csv of cars', async () => {
        await request(app)
            .post('/api/cars/upload')
            .attach('file', filePath)
            .expect(201)
    })

    test('GET /api/cars - lists all uploaded cars', async () => {
        await request(app)
            .get('/api/cars')
            .expect(200)
            .then(res => {
                expect(JSON.parse(res.text)).toMatchObject({
                    data: [
                        {
                            uuid: '377eed51-a6cd-4381-8729-e9c83ceb6df9',
                            vin: 'VH02',
                            make: 'Nissan',
                            model: 'Altima',
                            mileage: 14000,
                            year: 2000,
                            price: 20001,
                            zip: 55387,
                        },
                        {
                            uuid: '2b31e3a6-1e4a-4827-89ee-e7df37d409c2',
                            vin: 'VH03',
                            make: 'Nissan',
                            model: 'Altima',
                            mileage: 2,
                            year: 2001,
                            price: 50000,
                            zip: 55387,
                        },
                        {
                            uuid: '5319576b-6617-49b9-a1e9-3254c1195cca',
                            vin: 'VH04',
                            make: 'Honda',
                            model: 'CR-V',
                            mileage: 3,
                            year: 2002,
                            price: 20000,
                            zip: 55387,
                        },
                        {
                            uuid: 'bcbe1122-bd52-4d05-8fde-b408a1744d8b',
                            vin: 'VH05',
                            make: 'Honda',
                            model: 'CR-V',
                            mileage: 4,
                            year: 2003,
                            price: 20000,
                            zip: 55387,
                        },
                    ],
                })
            })
    })
})
