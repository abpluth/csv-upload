const express = require('express')
const swaggerUi = require('swagger-ui-express')
const routes = require('./routes')
const { swaggerSpec } = require('./swagger')

const app = express()

app.use('/api', routes)
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

module.exports = app
