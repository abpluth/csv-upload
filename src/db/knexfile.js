module.exports = {
    development: {
        client: 'pg',
        connection: {
            host: process.env.DB_HOSTNAME || 'localhost',
            user: process.env.POSTGRES_USER || 'postgres',
            password: process.env.POSTGRES_PASSWORD || 'secret',
        },
    },
    production: {
        client: 'pg',
        connection: {
            host: process.env.DB_HOSTNAME || 'postgres',
            user: process.env.POSTGRES_USER,
            password: process.env.POSTGRES_PASSWORD,
        },
    },
}
