const Knex = require('knex')
const environment = process.env.ENVIRONMENT || 'development'
const knexConfig = require('./knexfile')[environment]
const knex = Knex(knexConfig)

module.exports = knex
