const createSchema = async knex => {
    if (await knex.schema.hasTable('cars')) {
        return
    }

    await knex.schema.createTable('cars', table => {
        table.uuid('uuid').primary()
        table.string('vin').notNullable()
        table.string('make').notNullable()
        table.string('model').notNullable()
        table.integer('mileage').notNullable()
        table.integer('year').notNullable()
        table.integer('price').notNullable()
        table.integer('zip').notNullable()
        table.timestamps(true, true)
    })
}

module.exports = {
    createSchema,
}
