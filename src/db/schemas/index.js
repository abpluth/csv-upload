const cars = require('./cars')

const createSchemas = async knex => {
    await cars.createSchema(knex)
}

module.exports = {
    createSchemas,
}
