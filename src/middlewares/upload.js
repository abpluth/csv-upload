const multer = require('multer')
const uploadPath = require('path')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadPath.resolve() + '/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    },
})

const upload = multer({ storage })

module.exports = upload
