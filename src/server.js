const app = require('./app')
const { createSchemas } = require('./db/schemas')
const knex = require('./db')

const port = process.env.PORT || 3000

createSchemas(knex).then(() => {
    app.listen(port, () => {
        console.log(`Server Started on port ${port}`)
    })
})
