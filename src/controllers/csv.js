const csv = require('fast-csv')
const fs = require('fs')
const path = require('path')
const db = require('../db')

const BATCH_SIZE = 100
const UPDATE_FIELDS = [
    'vin',
    'make',
    'model',
    'mileage',
    'year',
    'price',
    'zip',
]
async function upload(req, res) {
    // TODO: add error handling
    let rows = []
    let errors = []
    const promises = []

    csv.parseFile(path.resolve() + '/uploads/' + req.file.filename, {
        headers: true,
    })
        .on('data', data => {
            try {
                rows.push({
                    uuid: data.uuid,
                    vin: data.vin,
                    make: data.make,
                    model: data.model,
                    mileage: data.mileage,
                    year: data.year,
                    price: data.price,
                    zip: data.zip,
                })
            } catch (e) {
                errors.push(e)
            }

            if (rows.length >= BATCH_SIZE) {
                promises.push(
                    db('cars')
                        .insert(rows)
                        .onConflict('uuid')
                        .merge(UPDATE_FIELDS),
                )
                rows = []
            }
        })
        .on('end', async () => {
            if (rows.length) {
                promises.push(
                    db('cars')
                        .insert(rows)
                        .onConflict('uuid')
                        .merge(UPDATE_FIELDS),
                )
                rows = []
            }

            // delete temp file
            fs.unlinkSync(req.file.path)
            await Promise.all(promises)
            res.status(201).json()
        })
}

async function getCars(req, res) {
    res.status(200).json({ data: await db('cars').select() })
}

module.exports = {
    upload,
    getCars,
}
