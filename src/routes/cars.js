const { Router } = require('express')
const csvController = require('../controllers/csv.js')
const upload = require('../middlewares/upload.js')
const routes = Router()

/**
 * @openapi
 * /api/cars/upload:
 *   post:
 *     description: Upload a csv file to the database
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               file:
 *                 type: string
 *                 format: binary
 *     produces:
 *       - application/json
 *     responses:
 *       201:
 *         description: success
 */
routes.post('/upload', upload.single('file'), csvController.upload)

/**
 * @openapi
 * /api/cars:
 *   get:
 *     description: Fetch all cars
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   uuid:
 *                     type: string
 *                     format: uuid
 *                   vin:
 *                     type: string
 *                   make:
 *                     type: string
 *                   model:
 *                     type: string
 *                   mileage:
 *                     type: integer
 *                   year:
 *                     type: integer
 *                   price:
 *                     type: integer
 *                   zip:
 *                     type: integer
 *                   created_at:
 *                     type: string
 *                     format: date-time
 *                   updated_at:
 *                     type: string
 *                     format: date-time
 */
routes.get('/', csvController.getCars)

module.exports = routes
