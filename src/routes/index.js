const { Router } = require('express')
const Cars = require('./cars')

const routes = Router()

routes.use('/cars', Cars)

module.exports = routes
